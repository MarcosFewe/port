import React from 'react';

//import components dos arquivos criados
import { FooterArea } from './styled';

const Footer = () => {
    return (
        <FooterArea>
            Todos os direitos reservados para b7web top ®
        </FooterArea>
    );
}

export default Footer;