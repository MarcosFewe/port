//importando biblioteca para criar um hash para a imagem e o manipulador de imagem
const { v4: uuid } = require('uuid');
const jimp = require('jimp');

const Category = require('../models/Category');
const User = require('../models/User');
const Ad = require("../models/Ad");

//pegar os dados em bytes da imagem, manipular e salvar
const addImage = async (buffer) => {
    let newName = `${uuid()}.jpg`;
    //pegando a imagem
    let tmpImg = await jimp.read(buffer);
    //redimensionando, definindo a qualidade e definindo o caminho onde será salva a imagem
    tmpImg.cover(500, 500).quality(80).write(`./public/media/${newName}`);
    return newName;
}

module.exports = {
    getCategories: async (req, res) => {
        const cats = await Category.find();

        let categories = [];
        for(let i in cats)
            categories.push({
                ...cats[i]._doc,
                img: `${process.env.BASE}/assets/images/${cats[i].slug}.png`
            });

        res.json({categories});
    },
    addAction: async (req, res) => {
        //recebendo os dados da req.body
        let { title, price, priceneg, desc, cat, token } = req.body;
        //buscando o user do token
        const user = await User.findOne({token}).exec();
        
        //verificações
        if(!title || !cat) {
            res.json({error: 'Titulo e/ou categoria não foi preenchido.'});
            return;
        }
        if(price) { //price ira chegar pelo req.body assim: 'R$ 8.000,50', ficará assim: 8000.50 para armazenar no banco
            price = price.replace('.', '').replace(',', '.').replace('R$ ', '');
            price = parseFloat(price);
        } else {
            price = 0;
        }

        const newAd = new Ad();
            newAd.status = true;
            newAd.idUser = user._id;
            newAd.state = user.state;
            newAd.dateCreated = new Date();
            newAd.title = title;
            newAd.category = cat;
            newAd.price = price;
            //verificar o priceneg pois ele é verdade ou falso, o valor vem dentro de string 'true'
            newAd.priceNegotiable = (priceneg=='true') ? true : false;
            newAd.description = desc;
            newAd.views = 0;

            //verificando se enviou imagens
            if(req.files && req.files.img) {
                //verificando se enviou uma imagem se não enviou mais imagens
                if(req.files.img.length == undefined) {
                    //verificando se as imagens estão no padrão dos mimetypes definidos
                    if(['image/jpeg', 'image/jpg', 'image/png'].includes(req.files.img.mimetype)) {
                        //após tratativa na const addImage de cada imagem, passa o valor em bytes de cada imagem na requisição da função addImage()
                        let url = await addImage(req.files.img.data);
                        //pegando as imagens em forma de objeto para salvar depois no banco
                        newAd.images.push({
                            url,
                            //default: false para não deixar imagem como a capa
                            default: false,
                        });
                    }
                } else {
                    //for para cada arquivo enviado na req
                    for(let i=0; i < req.files.img.length; i++) {
                        //verificando se as imagens estão no padrão dos mimetypes definidos
                        if(['image/jpeg', 'image/jpg', 'image/png'].includes(req.files.img[i].mimetype)) {
                            //após tratativa na const addImage de cada imagem, passa o valor em bytes de cada imagem na requisição da função addImage()
                            let url = await addImage(req.files.img[i].data);
                            //pegando as imagens em forma de objeto para salvar depois no banco
                            newAd.images.push({
                                url,
                                //default: false para não deixar imagem como a capa
                                default: false,
                            });
                        }
                    }
                }
            }
            //verificando se há mais que 0 imagens para então definir a primeira como default: true
            if(newAd.images.length > 0) {   
                newAd.images[0].default = true;
            }

            const info = await newAd.save();
            res.json({id: info._id});

    },
    getList: async (req, res) => {
        
    },
    getItem: async (req, res) => {
        
    },
    editAction: async (req, res) => {
        
    }
    
};